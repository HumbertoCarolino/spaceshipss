#include <iostream>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/Audio.hpp"
#include "SFML/System.hpp"
#include "SFML/Network.hpp"
#include <math.h>
#include <cstdio>
#include <vector>

// using SF so that we dont have to keep on using sf:: on everything
using namespace sf;
//making a class for the bullet
class Bullet
{
public:
	// a sprite is a drawn figure/texture
	Sprite shape;
	//giving the bullet its texture + setting position
	Bullet(Texture *texture, Vector2f pos)
	{
		this->shape.setTexture(*texture);

		this->shape.setScale(0.07f, 0.07f);

		this->shape.setPosition(pos);
	}

	~Bullet(){}
};
//making a class for the player
class Player
{
public:
	// giving a shape and texture the to player because its not an object that is built wwithin Visual Studio
	Sprite shape;
	Texture *texture;
	
	//making an HP intenger for the player
	int HP;
	int MaxHP;

	std::vector<Bullet> bullets;

	Player(Texture *texture)
	{
		// giving a maximum hp to the player
		this->MaxHP = 10;
		this->HP = this->MaxHP;

		//giving a texture to the HP of the player and setting its size
		this->texture = texture;
		this->shape.setTexture(*texture);

		this->shape.setScale(0.1f, 0.1f);
	}
	//player destructor
	~Player() {}

};
// making a class for the enemy 
class Enemy
{
public:

	//giving a spirte/render for our enemy
	Sprite shape;
	//applying the same HP method for the enemy
	int HP;
	int HPMax;

	Enemy(Texture *texture, Vector2u windowSize)
	{
		//same logic as the player giving a size and also setting position for our HP text
	//giving it a random number of hp from 1 to 3 
		this->HPMax = rand() % 3 + 1;
		this->HP = this->HPMax;

		this->shape.setTexture(*texture);

		this->shape.setScale(0.1f, 0.1f);
		//putting the shape of the enemy within a random position of our game boundries.
		this->shape.setPosition(windowSize.x - this->shape.getGlobalBounds().width, rand()% (int)(windowSize.y - this->shape.getGlobalBounds().height));
	}
	//enemy destructor
	~Enemy(){}
};

int main()
{
	//turning off the time in our game so that it doesnt move too fast
	srand(time(NULL));

	//Starting making the Game loop/initialisation "required function"
	RenderWindow window(VideoMode(800, 600), "Spaceship action!", Style::Default);
	//setting the speed for our game to move on
	window.setFramerateLimit(60);

	//creating a font for the game
	Font font;
	font.loadFromFile("Fonts/monster.ttf");

	//loading textures for every assets/ objects of the game "required function"
	Texture playerTex;
	playerTex.loadFromFile("Textures/spaceship.png");

	Texture enemyTex;
	enemyTex.loadFromFile("Textures/enemy.png");

	Texture bulletTex;
	bulletTex.loadFromFile("Textures/Bullets.png");

	//UI initialiser which is giving the colour, size and position for each one of game texts "required function"
	Text scoreText;
	scoreText.setFont(font);
	scoreText.setCharacterSize(20);
	scoreText.setFillColor(Color::White);
	scoreText.setPosition(10.f, 10.f);

	Text gameOverText;
	gameOverText.setFont(font);
	gameOverText.setCharacterSize(30);
	gameOverText.setFillColor(Color::Red);
	gameOverText.setPosition(100.f, window.getSize().y / 2);
	gameOverText.setString("GAME OVER!");

	//Starting our player with all its functions and giving it the HP required, making the player and giving it its texture
	int score = 0;
	Player player(&playerTex);
	int shootTimer = 20;
	// making a UI for the HP of the player
	Text hpText;
	hpText.setFont(font);
	hpText.setCharacterSize(12);
	hpText.setFillColor(Color::White);

	//giving our enemy a vector and calling them "enemies and setting the Timer for our enemy to spawn
	int enemySpawnTimer = 0;
	std::vector<Enemy> enemies;
	// Same as the Player HP
	Text eHpText;
	eHpText.setFont(font);
	eHpText.setCharacterSize(12);
	eHpText.setFillColor(Color::White);
	
	// the Game loop/initialisation "required function"
	while (window.isOpen())
	{
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		// if the player hasnt died the player can still move, this keeps the game running
		if (player.HP > 0)
		{
			//controlers for the player which is moving the shape of our player "required function"
			if (Keyboard::isKeyPressed(Keyboard::W))
				player.shape.move(0.f, -10.f);
			if (Keyboard::isKeyPressed(Keyboard::A))
				player.shape.move(-10.f, 0.f);
			if (Keyboard::isKeyPressed(Keyboard::S))
				player.shape.move(0.f, 10.f);
			if (Keyboard::isKeyPressed(Keyboard::D))
				player.shape.move(10.f, 0.f);
			//setting the position of the HP of the player which we want it to be right next to him + giving some text 
			hpText.setPosition(player.shape.getPosition().x, player.shape.getPosition().y - hpText.getGlobalBounds().height);
			//giving the numbers to our HP
			hpText.setString(std::to_string(player.HP) + "/" + std::to_string(player.MaxHP));

			//keeping the player inside the boundries of the window
			if (player.shape.getPosition().x <= 0) //getting the left boundry 
				player.shape.setPosition(0.f, player.shape.getPosition().y);
			if (player.shape.getPosition().x >= window.getSize().x - player.shape.getGlobalBounds().width) //getting the right boundry
				player.shape.setPosition(window.getSize().x - player.shape.getGlobalBounds().width, player.shape.getPosition().y);
			if (player.shape.getPosition().y <= 0) //the top limit
				player.shape.setPosition(player.shape.getPosition().x, 0.f);
			if (player.shape.getPosition().y >= window.getSize().y - player.shape.getGlobalBounds().height) //the bottom limit
				player.shape.setPosition(player.shape.getPosition().x, window.getSize().y - player.shape.getGlobalBounds().height);

			//Setting the timer for each bullet
			if (shootTimer < 15)
				shootTimer++;

			if (Mouse::isButtonPressed(Mouse::Left) && shootTimer >= 15) //getting input for the bullet
			{
				player.bullets.push_back(Bullet(&bulletTex, player.shape.getPosition()));
				shootTimer = 0; //putting a bullet timer which will depend on the frames of the game
			}

			//making the bullets size
			for (size_t i = 0; i < player.bullets.size(); i++)
			{
				//making the shape of the bullet and moving it
				player.bullets[i].shape.move(20.f, 0.f);

				//breaking bullets that go out of the window
				if (player.bullets[i].shape.getPosition().x > window.getSize().x)
				{
					player.bullets.erase(player.bullets.begin() + i);
					break;
				}

				//setting collusion boundries to the enemy "required function"
				for (size_t k = 0; k < enemies.size(); k++)
				{
					if (player.bullets[i].shape.getGlobalBounds().intersects(enemies[k].shape.getGlobalBounds()))
					{
						if (enemies[k].HP <= 1)
						{
							score += enemies[k].HPMax;
							enemies.erase(enemies.begin() + k);
						}
						else
							enemies[k].HP--; //making dmg to the enemy

						player.bullets.erase(player.bullets.begin() + i);
						break;
					}
				}
			}

			//putting a spawn timer on enemies "required function"
			if (enemySpawnTimer < 25)
				enemySpawnTimer++;

			//enemy spawn "required function"
			if (enemySpawnTimer >= 25)
			{
				enemies.push_back(Enemy(&enemyTex, window.getSize()));
				enemySpawnTimer = 0; //reseting the timer to 0 
			}

			for (size_t i = 0; i < enemies.size(); i++)
			{
				enemies[i].shape.move(-6.f, 0.f);

				if (enemies[i].shape.getPosition().x <= 0 - enemies[i].shape.getGlobalBounds().width)
				{
					enemies.erase(enemies.begin() + i);
					break;
				}

				if (enemies[i].shape.getGlobalBounds().intersects(player.shape.getGlobalBounds()))
				{
					enemies.erase(enemies.begin() + i);

					player.HP--; //if an enemy hits the player lose HP
					break;
				}
			}

			//UI Update
			scoreText.setString("Score: " + std::to_string(score));
		}

		
		window.clear();

		//drawing/rendering the player into the screen "required function"
		window.draw(player.shape);

		//giving a size and drawing the bullets onto the screen "required function"
		for (size_t i = 0; i < player.bullets.size(); i++)
		{
			window.draw(player.bullets[i].shape);
		}

		//drawing the enemies HP into the screen "required function"
		for (size_t i = 0; i < enemies.size(); i++)
		{
			eHpText.setString(std::to_string(enemies[i].HP) + "/" + std::to_string(enemies[i].HPMax));
			eHpText.setPosition(enemies[i].shape.getPosition().x, enemies[i].shape.getPosition().y - eHpText.getGlobalBounds().height);
			window.draw(eHpText);
			window.draw(enemies[i].shape);
		}

		///drawing the UI into the game when players hp reaches 0 "required function"
		window.draw(scoreText);
		window.draw(hpText);

		if (player.HP <= 0)
			window.draw(gameOverText);

		window.display();
	}

	return 0;
}